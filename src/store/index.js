import { createStore, applyMiddleware, combineReducers} from 'redux'
import { fetchData, searchText } from './reducer'
import { loadingDataMiddleWare } from './middleWares'

const store = createStore(fetchData, applyMiddleware(loadingDataMiddleWare))
export default store