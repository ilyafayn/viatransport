import { DATA_FETCHING_OBJ, SEARCH_ACTION } from '../constants'
export const LoadingData = () => {
    return {type: DATA_FETCHING_OBJ.DATA_LOADING}
}

export const LoadingDataError = (err) =>{
    return {type: DATA_FETCHING_OBJ.DATA_LOADING_ERROR, payload: err}
}
export const LoadData = (parsedData) => {
    return {
        type: DATA_FETCHING_OBJ.DATA_LOADED, payload: parsedData
    }
}

export const SearchText = (value) => {
    return {
        type: SEARCH_ACTION,
        payload: value
    }
}