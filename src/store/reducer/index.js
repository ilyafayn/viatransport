import { DATA_FETCHING_OBJ, SEARCH_ACTION } from '../constants'
export const fetchData = (state = {}, action) => {
    switch (action.type){
        case DATA_FETCHING_OBJ.DATA_LOADING:
            return {...state, isLoading: true}
        case DATA_FETCHING_OBJ.DATA_LOADED:
            return {
                ...state,
                isLoading: false,
                itemsList: action.payload
            }
        case DATA_FETCHING_OBJ.DATA_LOADING_ERROR:
            return {
                ...state,
                isLoading: false,
                isError: true
            }
        case SEARCH_ACTION:
            return {
                ...state,
                searchValue: action.payload
            }
        default:
            return state
    }
}

