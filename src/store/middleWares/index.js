import { DATA_FETCHING_OBJ } from '../constants'
import { LoadData, LoadingDataError } from '../actions'
export const loadingDataMiddleWare = ({ dispatch, getState }) => {
    return next => action => {
    switch (action.type){
        case DATA_FETCHING_OBJ.DATA_LOADING:
            return fetch('http://private-05627-frontendnewhire.apiary-mock.com/contact_list')
                .then((data)=>data.json())
                .then((parsedData)=>{
                    dispatch(LoadData(parsedData))
                    return Promise.resolve(getState());
                })
                .catch((err)=>{dispatch(LoadingDataError(err))})
    }
    return next(action)
    }
}
