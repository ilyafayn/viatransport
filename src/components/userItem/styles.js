import styled from 'styled-components'

export const DetailsHoverWrapper = styled.div`
    height: 60px;
    transition: .3s height;
    &  .hoverClass{    
        transition: .3s opacity;  
        opacity: 0;
    }
`

export const UserItemWrapper = styled.div`
    width: 200px;
    height: 378px;
    padding: 10px;
    position: relative;
    background-color: #ffffff;
    overflow: hidden;
    border-radius: 5px;
    box-shadow: -4px 10px 5px -6px rgba(179,179,179,1);
    &:hover{
        cursor: pointer;
        ${DetailsHoverWrapper}{
            &  .hoverClass{
                opacity: 1;
            }
            height: 120px;
        }
    }
`

export const ImageWrapper = styled.div`
    width: 100%;
    max-height: 300px;
    overflow: hidden;
`

export const Image = styled.img`
    width: 100%;
    height: auto;
`

export const DetailsWrapper = styled.div`
    position: absolute;
    width: 100%;
    bottom: 0;
    background-color: #ffffff;
`

export const IconWrapper = styled.div`
    height: 30px;
    width: 30px;
    border-radius: 30px;
    position: absolute;
    top: 0px;
    left: 5%;
    margin: 0;
    transform: translateY(-54%);
    padding: 0;
`
export const DescriptionWrapper = styled.div`
    display: flex;
    flex-direction: column;
    margin-top: 30px;
    & h3{
        margin-bottom: 5px;
        padding: 0 5%;
    }
`
export const DriversInfo = styled.div`
    padding: 0 5%;
    overflow-wrap: break-word;
    display: flex;
    flex-direction: column;
    font-size: 12px;
    & div{
        white-space: nowrap;
    }
`