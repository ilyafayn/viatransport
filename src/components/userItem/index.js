import React from 'react'
import PropTypes from 'prop-types'
import { UserItemWrapper, Image, ImageWrapper, DetailsWrapper, IconWrapper, DescriptionWrapper, DriversInfo, DetailsHoverWrapper } from './styles'
import DriverType from './driverType'

const default_image = 'http://s3.amazonaws.com/37assets/svn/765-default-avatar.png'
const UserItem = (props) => {
    const { name, driverType, driverRank, phone, email, profile_image } = props
    return <UserItemWrapper>
        <ImageWrapper>
            <Image  src={profile_image || default_image} />
        </ImageWrapper>
        <DetailsWrapper>
            <DetailsHoverWrapper>
                <IconWrapper>
                    {DriverType(driverType)}
                </IconWrapper>
                <DescriptionWrapper>
                    <h3>{name}</h3>
                    <DriversInfo>
                        <div>Driver Rank: {driverRank}</div>
                        <div className='hoverClass'>Phone number: {phone}</div>
                        <div className='hoverClass'>Email: {email}</div>
                    </DriversInfo>
                </DescriptionWrapper>
            </DetailsHoverWrapper>
        </DetailsWrapper>
    </UserItemWrapper>
}
UserItem.propTypes = { 
    name: PropTypes.string,
    driverType: PropTypes.string,
    driverRank: PropTypes.string,
    phone: PropTypes.string,
    email: PropTypes.string,
    profile_image: PropTypes.string
}
export default UserItem
