import styled from 'styled-components'
export const HeaderWrapper = styled.div`
    width: 100%;
    background-color: #20B8EA;
    padding: 10px;

`
export const InputWrapper = styled.div`
    width: fit-content;
    position: relative;
    place-self: end;
    padding-right: 40px;
    & input{
        border: none;
        padding: 5px;
        border-radius: 5px;
    }
`

export const SearchWrapper = styled.div`
    display: grid;
    grid-template-columns: 70% 30%;
    & h1{
        color: #ffffff;
        margin-left: 50px;
        font-size: 16px;
    }
`

export const IconSearchWrapper = styled.div`
    position: absolute;
    top: 0;
    right: 40px;
    transform: scale(.6);
    &:hover{
        cursor: pointer;
    }
`