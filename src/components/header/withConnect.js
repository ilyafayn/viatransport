import { connect } from 'react-redux'
import { SearchText } from '../../store/actions'
const mapStateToProps = (state)=>{

}

const mapDispatchToProps = dispatch => {
    return {
        setSearchValue: (value) => dispatch(SearchText(value))
    }
}

export const withConnect = (obj) => {
    return connect(mapStateToProps, mapDispatchToProps)(obj)
}
