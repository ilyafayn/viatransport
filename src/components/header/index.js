import React, { useState } from 'react'
import { HeaderWrapper, InputWrapper, SearchWrapper, IconSearchWrapper } from './styles'
import { IconSearch } from './iconSearch'
import { withConnect } from './withConnect'


const Header = ({setSearchValue}) => {
    const onClickAction = () => {
        setSearchValue(inputValue)
    }
    const onInputChange = (event) => {
        setInputValue(event.target.value)
    }
    const keyPressed = (event) => {
        if (event.key === "Enter") {
            setSearchValue(inputValue)
        }
    }
    const [inputValue, setInputValue] = useState('')
    return <HeaderWrapper>
        <SearchWrapper>
            <h1>Contact List</h1>
            <InputWrapper>
                <input type='text' placeholder='Please type user name' onKeyPress={keyPressed} onChange={onInputChange}  value={inputValue}/>
                <IconSearchWrapper onClick={onClickAction}>
                    <IconSearch/>
                </IconSearchWrapper>
            </InputWrapper>
        </SearchWrapper>
    </HeaderWrapper>
}

export default withConnect(Header)