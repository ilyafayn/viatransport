import React from 'react'
import UserItem from '../userItem'
import { withConnect } from './withConnect'
import { ItemsListWrapper, LoaderWrapper } from './styles'

const ItemsList = ({itemsList, searchValue}) => {
    const filteredValue = itemsList && itemsList.filter((elem)=>{
        return elem.name.includes(searchValue)
    })
    const arrToPopulate = searchValue ? filteredValue : itemsList
    return <ItemsListWrapper>
        {arrToPopulate && arrToPopulate.map((elem) => {
             return <UserItem {...elem} />
         })}
         
    </ItemsListWrapper>
}
export default withConnect(ItemsList)

