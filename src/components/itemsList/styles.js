import styled from 'styled-components'

export const ItemsListWrapper = styled.div`
    display: grid;
    flex-wrap: wrap;
    justify-items: center;
    gap: 10px;
    max-width: 1000px;
    margin: 40px auto 0 auto; 
    @media screen and (max-width: 768px) {
        grid-template-columns: auto auto;
    }
    @media screen and (min-width: 768px) and (max-width: 1023px) {
        grid-template-columns: auto auto auto;
    }
    @media screen and (min-width: 1024px) {
        grid-template-columns: auto auto auto auto;
    }

`