import { connect } from 'react-redux'

const mapStateToProps = (state)=>{
    const { itemsList, searchValue } = state
    return { itemsList, searchValue }
}

export const withConnect = (obj) => {
    return connect(mapStateToProps)(obj)
}
