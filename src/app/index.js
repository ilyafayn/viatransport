import React from 'react'
import Header from '../components/header'
import { Provider } from 'react-redux'
import store from '../store'
import ItemsList from '../components/itemsList'
import { GlobalStyle } from './styles'
 
const App = () => {
    React.useEffect(()=>{
        store.dispatch({type: 'DATA_LOADING'})
    },[])
    return <div className='main'>
        <Provider store={store}>
            <GlobalStyle />
             <Header />
             <ItemsList />
        </Provider>
    </div>
}
export default App